"use client";
import "./globals.css"
import React from "react";
import localFont from '@next/font/local';
import { useUserAgent } from 'next-useragent';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { grey } from '@mui/material/colors';
import {ThemeContext} from "../components/ThemeContext.js"
import BgRectangles from "../components/BgRectangles.js";
import Map from "../components/Map.js";

const myFont = localFont({ src: 'GoldmanSans_Th.ttf' });
const theme = createTheme({
    palette: {
        primary: {
            main: grey["A100"],
        },
        text:{
            primary: "#000000",
            secondary: "#000000"
        },
    },
    typography: {
        fontFamily: "var(--font-inter)",
    }

});

export default function RootLayout({children}) {
    const [ua, setUa] = React.useState({})

  React.useMemo(() => {
      if (typeof window != "undefined") {
          setUa(useUserAgent(window.navigator.userAgent))
      }
    },[])

    return (
            <html lang="en" className={myFont.className}>
                <head />
                <body>
                    <ThemeContext.Provider value={ua}>
                    <ThemeProvider theme={theme}>
                        <BgRectangles/>
                        <Map/>
                        {children}
                    </ThemeProvider>
                    </ThemeContext.Provider>
                </body>
            </html>
            )
}
