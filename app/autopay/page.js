"use client";
import PrimaryText from "../../components/PrimaryText";
import Link from "next/link";
import React from 'react';
import {Login} from "../../components/Login";
import {AdminPanel} from "../../components/AdminPanel";

export default function Page(){
    return(
            <>
            <PrimaryText childrenMenu={
                <>
                <Link href="/" alt={"10144"}>{"<< autopay: on"}</Link> <br/>
                </>
            }
            childrenBody={
                <>
                {(true) ? (<AdminPanel/>):(<Login/>)}
            </>
            }/>
            </>
    )
}
