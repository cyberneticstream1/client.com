import Grid from "@mui/material/Unstable_Grid2";
import Box from "@mui/material/Box";
import {IOSSlider, IOSSwitch, marks} from "./IOSFormComponents";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormGroup from "@mui/material/FormGroup";
import Stack from "@mui/material/Stack";
import {Button} from "@mui/material";
import Tooltip from "@mui/material/Tooltip";
import IconButton from "@mui/material/IconButton";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";

export function AdminPanel(){

    return(
            <>
            <Box container sx={{width:"100%"}} className={"mx-auto "}>
                <Grid container>
                    <Grid className={""}  sx = {{width:"50%" }}>
                        <Grid className={" relative  left-[28%] "} sx = {{width:50, height:36 }}>

                            <FormGroup><Stack direction ={"row"}>
                                <FormControlLabel control={ <IOSSwitch className={"my-1"} sx={{ m: 1 }} defaultChecked size = "large"/>} label={<><Tooltip title={"autopay setting"}><p>autopay</p></Tooltip></>}/>

                            </Stack>
                            </FormGroup>
                        </Grid>
                    </Grid>
                    <Grid className={""} sx = {{width:"50%" }}>
                        <Grid className={" w-fit fixed right-[14%]"}  >
                            <Button variant="contained" disableElevation color={"primary"}>
                                link bank </Button>
                        </Grid>
                    </Grid>

                    <Grid sx = {{width:500 }}className={"mx-auto "}>

                        <Box sx={{ width: "73%" }} className={"mt-8 mx-auto "}  >
                            <Stack direction={"row"} spacing={1.5}>
                                <IOSSlider
                                    aria-label="slider"
                                    min = {-15}
                                    max = {5}
                                    marks={marks}
                                    step={1}
                                    valueLabelDisplay="on"
                                />
                                <Tooltip title={"for autopay and without"} placement={"right-start"} >
                                    <p className={"pt-1"}>timing</p>
                                </Tooltip>
                            </Stack>
                        </Box>
                    </Grid>
                </Grid>
            </Box>
            <Box className={"fixed bottom-0 left-1/2 -translate-x-1/2"}>
                <Tooltip title="users" className={""}>
                    <IconButton >
                        <AdminPanelSettingsIcon />
                    </IconButton>
                </Tooltip>
            </Box>

            </>
            )
}