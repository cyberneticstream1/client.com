import Stack from "@mui/material/Stack";
import {Button} from "@mui/material";

export function Login() {
    return (
            <Stack direction="column" spacing={1.5} className={"mx-4 my-3"}>
                <Button onClick={(x) => console.log(x)} variant="contained" disableElevation color={"primary"}>
                    cyberneticstream@gmail.com
                </Button>
                <Button onClick={(x) => console.log(x)} variant="contained" disableElevation color={"primary"}>
                    cyberneticstream@icloud.com
                </Button>
                <Button onClick={(x) => console.log(x)} variant="contained" disableElevation color={"primary"}>
                    samuelbegie21@gmail.com
                </Button>
            </Stack>
            );
}